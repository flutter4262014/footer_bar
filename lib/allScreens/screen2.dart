import 'package:flutter/material.dart';

class Screen2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        // The scrollable content for Screen 2
        ListTile(title: Text('Item 1')),
        ListTile(title: Text('Item 2')),
        ListTile(title: Text('Item 3')),
        ListTile(title: Text('Item 4')),
        ListTile(title: Text('Item 5')),
        ListTile(title: Text('Item 6')),
        ListTile(title: Text('Item 7')),
        ListTile(title: Text('Item 8')),
        ListTile(title: Text('Item 9')),
        ListTile(title: Text('Item 10')),
        ListTile(title: Text('Item 11')),
        ListTile(title: Text('Item 12')),
        ListTile(title: Text('Item 13')),
        ListTile(title: Text('Item 14')),
      ],
    );
  }
}
