# footer_bar

Creating an application which has a footer bar with clickable elements (icons) in Flutter.

Each of the clickable elements redirects to a seperate screen.

The code for each screen is placed in a separate Dart file.

